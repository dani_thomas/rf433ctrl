#!/usr/bin/env python
#-------------------------------------------------------------------------------
# Name:        Receive RF 433 mhz signals
#              
# Purpose:     Class to recieve and in future send RF 433 mhz signals using
#              different protocols for the Raspberry pi
#
# Author:      Dani Thomas
#
# Usage:       Either call without parameters and it will listen for signals 
#              indefinitely or with -secs <number> which will recieve for a
#              a given number of seconds. Can be safely stopped with Ctrl-C
# Requires:    
# Based on:    
#-------------------------------------------------------------------------------

from RF433Ctrl import RFCtrl
import argparse
import signal
from datetime import datetime

def quit_gracefully(*args):
  print ("")
  print "Exiting gracefully"
  lRFCtrl.CleanupPins()
  exit(0)
    
if __name__ == "__main__":
  ap = argparse.ArgumentParser()
  ap.add_argument("-secs", action='store', help="how long to run")
  args=ap.parse_args()
  
  signal.signal(signal.SIGINT, quit_gracefully)
  
  lRFCtrl = RFCtrl()
  lRFCtrl.SetupPin()
  
  print "Listening for RF signals"
  
  cumulative_time=0
  start_time=datetime.now()
  
  while args.secs==None or cumulative_time < int(args.secs):
    number= lRFCtrl.ReceiveMessages()
    if number<>None and number<>'':
      print (int(number,2))
    cumulative_time = (datetime.now() - start_time).seconds
   
