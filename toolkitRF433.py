#!/usr/bin/env python
#-------------------------------------------------------------------------------
# Name:        Toolkit for RF 433 mhz
#              
# Purpose:     A number of tools to discover the protocol and codes used
#              by a 433mhz RF transmitter. Using this will help create a
#              protocol.json file for receiving and transmitting RF signals
#
# Author:      Dani Thomas
#
# Usage:       The following parameters are accepted
#              -rec      will record raw signals from 433mhz receiver
#              -read     <filename> will read values from file
#                        use - for default filename
#              -write    <filename> option to write the values to file
#                        use - for default filename
#              -trim     will remove low signal at the beginning and
#                        smooth out signals of less than 20u
#              -mean     works out the mean signal values and applies
#                        across the signal thereby rationalizing
#              -block    will attempt to find repeating blocks
#              -display  displays signal lengths in ms as /HIGH\ and _LOW_
#              -plot     alternatively plots the values to graph
#
#              if using -rec use sudo as requires access to GPIO pins
#                  sudo python toolkitRF433.py -rec
#              if using -plot you may need to use
#                  gksudo -- python toolkitRF433.py -read - -plot
#
# Requires:    Some options require matplotlib
# Based on:    
#-------------------------------------------------------------------------------

import sys
import argparse
from RF433Ctrl import RFCtrl
import matplotlib.pyplot as pyplot

LOW=0
HIGH=1

#-------------------------------------------------------------------------------

def ReadFromFile(fileName):
  ReceivedRaw = [[], []]
  intString=''
  f = open(fileName,'r')
  char= f.read(1)
  while not(char == ""):
    if char=="\\":
      ReceivedRaw[0].append(int(intString))
      ReceivedRaw[1].append(HIGH)
      intString=""
    elif char=="_" and len(intString) > 0:
      ReceivedRaw[0].append(int(intString))
      ReceivedRaw[1].append(LOW)
      intString=""
    elif char.isdigit():
      intString=intString + char
    char= f.read(1)
  return ReceivedRaw 

#-------------------------------------------------------------------------------

def WriteToFile(fileName, listOfTuple, length=None):
  f = open(fileName,'w')
  if length==None:
    length=len(listOfTuple[0])
  for i in range(length):
    if listOfTuple[1][i]==LOW:
      delim="_{0}_"
    else:
      delim="/{0}\\"           
    f.write(delim.format(listOfTuple[0][i]))
  f.close()

#-------------------------------------------------------------------------------
  
def printListOfTuple(title, listOfTuple, length=None):
  if length==None:
    length=len(listOfTuple[0])
  print (title)
  for i in range(length):
    if listOfTuple[1][i]==LOW:
      delim="_{0}_"
    else:
      delim="/{0}\\"           
    sys.stdout.write(delim.format(listOfTuple[0][i]))
  print ("")

#-------------------------------------------------------------------------------

def trimReceivedSignal(ReceivedRaw):
   minimum_length=150
   maximum_length=10000
   trimmedSignal = [[], []]
   trimmedSignal2 = [[], []]
   for i in range(len(ReceivedRaw[0])):
     if len(trimmedSignal[0]) > 0 and (ReceivedRaw[0][i] < minimum_length or ReceivedRaw[1][i]==trimmedSignal[1][-1]):
       trimmedSignal[0][-1]=trimmedSignal[0][-1] + ReceivedRaw[0][i]
     else:
       trimmedSignal[0].append(ReceivedRaw[0][i])
       trimmedSignal[1].append(ReceivedRaw[1][i])
   
   for i in range(len(trimmedSignal[0])):
     if trimmedSignal[0][i] < maximum_length:
        trimmedSignal2[0].append(trimmedSignal[0][i])
        trimmedSignal2[1].append(trimmedSignal[1][i])
    
   return trimmedSignal2 

#-------------------------------------------------------------------------------

def getMeanValues( Signal ):
      param=0.01  
      means=[[],[]]
      tolerance=140
      for i in range(len(Signal[0])):
        found=False
        for z in range(len(means[0])):
          if abs(Signal[0][i] - means[0][z]) < tolerance:
            means[0][z]=means[0][z]*(1-param) + Signal[0][i] * param
            means[1][z]=means[1][z]+1
            found=True
            break
        if not(found) and len(means[0]) < 5:
          means[0].append(Signal[0][i])
          means[1].append(1)
       
      return means

#-------------------------------------------------------------------------------
      
def applyMeanValues(Signal,  means,  limit):    
      for i in range(len(Signal[0])):
        smallestDiverge=9999
        closestMean=0
        for z in range(0,limit):
          diverge =abs(Signal[0][i] - means[0][z])
          if diverge < smallestDiverge:
            smallestDiverge=diverge
            closestMean=z        
        Signal[0][i]=int(means[0][closestMean])
      return Signal
      
#-------------------------------------------------------------------------------

def getRepeatedBlock(Signal):
      
      blockLength=[]   
      for i in range(2,len(Signal[0])/2):
        if Signal[0][0:i]== Signal[0][i:2*i]:
          blockLength.append(i)
      
      return blockLength

#-------------------------------------------------------------------------------
        
def plotListOfTuple(listOfTuple):
  CumulativeSignal = [[], []]
  print ('**Processing results**')
  cumulativeTime=0
  
  for i in range(len(listOfTuple[0])):
    
    for s in range(0,listOfTuple[0][i]):
      CumulativeSignal[0].append(cumulativeTime + s)
      CumulativeSignal[1].append(listOfTuple[1][i])
    cumulativeTime = cumulativeTime+listOfTuple[0][i]
   
  print ('**Plotting results**')
  pyplot.plot(CumulativeSignal[0], CumulativeSignal[1])
  pyplot.axis([0, cumulativeTime, -1, 2])
  pyplot.show()


if __name__ == "__main__":
  ap = argparse.ArgumentParser()
  ap.add_argument("-rec", action='store_true', help="want to record")
  ap.add_argument("-read", action='store',help="read from file")
  ap.add_argument("-write", action='store', help="write to file")
  ap.add_argument("-trim", action='store_true',help="trim the raw file")
  ap.add_argument("-mean", action='store_true',help="get mean values from the raw file")
  ap.add_argument("-block", action='store_true',help="find repeating blocks")
  ap.add_argument("-display", action='store_true', help="display to screen")
  ap.add_argument("-plot", action='store_true', help="plot to graph")
  
  args = ap.parse_args()
  
  if not(args.rec) and args.read==None:
    print ("Needs either input from file (-read) or record (-rec)")
    exit(0)
  
  if args.mean:
    args.trim=True
    
  if args.block:
    args.trim=True
    args.mean=True
  
  if args.rec:
    print ("Start Recording for 5 seconds")
    lRFCtrl = RFCtrl()
    lRFCtrl.SetupPin()
    ReceivedMessage = lRFCtrl.getReceivedRaw(5)
    print ("Stopped")
  
  if args.read is not None:
    if args.read=='-':
       args.read="./receive.dat"
    print ("Reading from file: {0}".format(args.read))
    ReceivedMessage=ReadFromFile(args.read)
    print ("Finished reading")
    
  if args.trim:
    print ("Trimming raw signal")
    ReceivedMessage=trimReceivedSignal(ReceivedMessage)
    print ("Finished Trimming")
    
  if args.mean:
     meanValues = getMeanValues(ReceivedMessage)
     print ("Mean Values")
     for z in range(len(meanValues[0])):
       print ("{0}({1}) ".format(int(meanValues[0][z]),meanValues[1][z])),            
     print ("")
     ReceivedMessage=applyMeanValues( ReceivedMessage, meanValues, 5 )
  
  length=None
  if args.block:
     print ("Attempting to find repeating blocks")
     repeatedBlock = getRepeatedBlock(ReceivedMessage)
     if len(repeatedBlock) > 0:
       print ("({0} times)".format(len(repeatedBlock)))
       length = repeatedBlock[0]
     else:
       print ("None found")
         
  if args.display:
    printListOfTuple("Received Message", ReceivedMessage,length)
  
  if args.plot:
    plotListOfTuple(ReceivedMessage)
    
  if args.write is not None:
    if args.write=='-':
       args.write="./receive.dat"
    sys.stdout.write("Write to file: {0} Y/N? ".format(args.write))
    yesNo=sys.stdin.read(1)
    if yesNo=="Y" or yesNo=="y":
      print ("Writing ....")
      WriteToFile(args.write,ReceivedMessage,length)
      print ("Finished writing")
