RF433Ctrl

The Repository consists of a class for receiving 433mhz messages from a RF receiver connected to a Raspberry Pi. This is for the purpose of Home Automation. There is a protocol.json file which documents the protocol of the Nexa sockets and other products and also the Blyss Door Chime. To listen out for either of these signals use: sudo python receiveRF433Ctrl.py or with the optional -secs <seconds> if you want a time limit. You will need to use sudo as the program needs to access the GPIO. By default the class uses BCM Pin 27. If you are confused around pin numbering I suggest downloading wiringpi and running gpio readall. See several tutorials on the web on how to wire up receivers safely.

     pi@raspberrypi ~/RF433Ctrl $ gpio readall
     +-----+-----+---------+------+---+---Pi 2---+---+------+---------+-----+-----+
     | BCM | wPi |   Name  | Mode | V | Physical | V | Mode | Name    | wPi | BCM |
     +-----+-----+---------+------+---+----++----+---+------+---------+-----+-----+
     |     |     |    3.3v |      |   |  1 || 2  |   |      | 5v      |     |     |
     |   2 |   8 |   SDA.1 |   IN | 1 |  3 || 4  |   |      | 5V      |     |     |
     |   3 |   9 |   SCL.1 |   IN | 1 |  5 || 6  |   |      | 0v      |     |     |
     |   4 |   7 | GPIO. 7 |   IN | 1 |  7 || 8  | 1 | ALT0 | TxD     | 15  | 14  |
     |     |     |      0v |      |   |  9 || 10 | 1 | ALT0 | RxD     | 16  | 15  |
     |  17 |   0 | GPIO. 0 |   IN | 0 | 11 || 12 | 0 | IN   | GPIO. 1 | 1   | 18  |
     |  27 |   2 | GPIO. 2 |   IN | 0 | 13 || 14 |   |      | 0v      |     |     |
     |  22 |   3 | GPIO. 3 |   IN | 0 | 15 || 16 | 0 | IN   | GPIO. 4 | 4   | 23  |
     |     |     |    3.3v |      |   | 17 || 18 | 0 | IN   | GPIO. 5 | 5   | 24  |
     |  10 |  12 |    MOSI |   IN | 0 | 19 || 20 |   |      | 0v      |     |     |
     |   9 |  13 |    MISO |   IN | 0 | 21 || 22 | 0 | IN   | GPIO. 6 | 6   | 25  |
     |  11 |  14 |    SCLK |   IN | 0 | 23 || 24 | 1 | IN   | CE0     | 10  | 8   |
     |     |     |      0v |      |   | 25 || 26 | 1 | IN   | CE1     | 11  | 7   |
     |   0 |  30 |   SDA.0 |   IN | 1 | 27 || 28 | 1 | IN   | SCL.0   | 31  | 1   |
     |   5 |  21 | GPIO.21 |   IN | 1 | 29 || 30 |   |      | 0v      |     |     |
     |   6 |  22 | GPIO.22 |   IN | 1 | 31 || 32 | 0 | IN   | GPIO.26 | 26  | 12  |
     |  13 |  23 | GPIO.23 |   IN | 0 | 33 || 34 |   |      | 0v      |     |     |
     |  19 |  24 | GPIO.24 |   IN | 0 | 35 || 36 | 0 | IN   | GPIO.27 | 27  | 16  |
     |  26 |  25 | GPIO.25 |   IN | 0 | 37 || 38 | 0 | IN   | GPIO.28 | 28  | 20  |
     |     |     |      0v |      |   | 39 || 40 | 0 | IN   | GPIO.29 | 29  | 21  |
     +-----+-----+---------+------+---+----++----+---+------+---------+-----+-----+
     | BCM | wPi |   Name  | Mode | V | Physical | V | Mode | Name    | wPi | BCM |
     +-----+-----+---------+------+---+---Pi 2---+---+------+---------+-----+-----+
     pi@raspberrypi ~/RF433Ctrl $

The repository also contains a toolkit which should help to identify different protocols called toolkitRF433.py. This has two modes of usage. You can either use the command: sudo python toolkitRF433.py -rec -display. This will record 5 seconds of signals and then display in text format /High\_Low_/High\. Or alternatively you can install the matplotlib which displays the signal in graphical format. In this case you will need to use gksudo -- python toolkitRF433.py -rec -plot.

The complete set of parameters are as follows:

               -rec      will record raw signals from 433mhz receiver
               -read     <filename> will read values from file
                         use - for default filename
               -write    <filename> option to write the values to file
                         use - for default filename
               -trim     will remove low signal at the beginning and
                         smooth out signals of less than 20u
               -mean     works out the mean signal values and applies
                         across the signal thereby rationalizing
               -block    will attempt to find repeating blocks
               -display  displays signal lengths in ms as /HIGH\ and _LOW_
               -plot     alternatively plots the values to graph

To get started I would use the option sudo python toolkitRF433.py -rec -display to get a feel for things and see that you are getting some signal through. Then use gksudo -- python toolkitRF433.py -rec -plot to see where you are getting the signal amongst the noise. Then add the -write - option and run either of the previous commands again. If you have a fairly clean signal - you should see several blocks in the middle of the graph then press Y to confirm saving. Then instead of the -rec option use -read - to read the signal from the saved file. Then you could use -block option which will also run the -trim and -mean options and if you are lucky you will get a clean consistent small block of signal consiting of a latch followed by codes representing 1s and 0s. At any point you can edit the receive.dat file manually to cut out noise and repeating blocks.

The latest version I have added code for transmitting codes transmitRF433Ctrl.py and homeautomation.json. You will need to edit the json file to insert the codes and make sure that the pins have been setup in RF433Ctrl.py for your particular setup. As always let me know if there are any issues.