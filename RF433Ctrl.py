#-------------------------------------------------------------------------------
# Name:        RF 433 mhz controller
#              
# Purpose:     Class to recieve and in future send RF 433 mhz signals using
#              different protocols for the Raspberry pi
#
# Author:      Dani Thomas
#
# Requires:    
# Based on:    
#-------------------------------------------------------------------------------
#!/usr/bin/python
import sys
import os.path
import json
import RPi.GPIO as GPIO
import time
from datetime import datetime

class RFCtrl:
    PROTOCOL_FILENAME = os.path.dirname(os.path.realpath(__file__)) + "/protocols.json"
    protocolInfo = None
    receivePin = 6
    transmitPin=26
    MAX_DURATION = 5
    LOW = 0
    HIGH = 1
    #-----------------------------------------------------------------------------------------------
    def __init__( self ):
        
        self.scriptPath = os.path.dirname( __file__ )
        self.__tryToLoadProtocolFile()
        
    #-----------------------------------------------------------------------------------------------
    def __tryToLoadProtocolFile( self ):
        
        if os.path.exists( self.PROTOCOL_FILENAME ):
            try:
                with open( self.PROTOCOL_FILENAME ) as protocolFile:
                    
                    self.protocolInfo = json.load( protocolFile )
                               
            except Exception as e:
                print( "Unable to load protocol file. Exception was " + str( e ) )
        else:
            print("Unable to find path " + self.PROTOCOL_FILENAME)
    #-----------------------------------------------------------------------------------------------
    def __ReadDigits(self,protocol):
        ReceivedRaw = [[], []]  
        TheNumber=""
        digit=protocol["CodeBlock"]["Body"]["Digit"]
        for pbit in range(0, protocol["CodeBlock"]["Body"]["NumberOfBits"]):
          found=[0,0]
          for t in range(len(digit[0]["timings"])): 
            beginning_time=datetime.now()
            while GPIO.input(self.receivePin) == digit[0]["timings"][t]["Signal"]:
              pass
            timeDelta= (datetime.now() - beginning_time).microseconds
            ReceivedRaw[0].append(timeDelta)
            if abs(timeDelta  - digit[0]["timings"][t]["Length"]) < 100:
              found[0]=found[0]+1
            if abs(timeDelta  - digit[1]["timings"][t]["Length"]) < 100:
              found[1]=found[1]+1

          if found[0]==len(digit[0]["timings"]):
              TheNumber=TheNumber + str(digit[0]["value"])
          elif found[1]==len(digit[1]["timings"]):
              TheNumber=TheNumber + str(digit[1]["value"])
        
        if len(TheNumber) == 32:
          for i in range(len(ReceivedRaw[0])):
            print(ReceivedRaw[0][i])     
        return TheNumber
    #-----------------------------------------------------------------------------------------------
   
    def __GetLatch(self):
          while GPIO.input(self.receivePin) == self.LOW:
            pass
          found = False
          beginning_time=datetime.now()
          while GPIO.input(self.receivePin) == self.HIGH:
                pass
          timeHigh = datetime.now() - beginning_time
          beginning_time=datetime.now()
          while GPIO.input(self.receivePin) == self.LOW:
            pass  
          timeLow = datetime.now() - beginning_time
          for protocol in self.protocolInfo["Protocols"]:
            if abs(timeHigh.microseconds - protocol["CodeBlock"]["Latch"][0]["Length"]) < 100 and abs(timeLow.microseconds - protocol["CodeBlock"]["Latch"][1]["Length"]) < 100:
              return protocol
          return None

    #-----------------------------------------------------------------------------------------------
          
    def __DoEnding(self, protocol):
      found=0
      for ending in self.protocolInfo["Protocols"][0]["CodeBlock"]["Ending"]:
        beginning_time = datetime.now()
        while GPIO.input(self.receivePin) == ending["Signal"]:
          pass            
        timeDelta = datetime.now() - beginning_time
        if abs(timeDelta.microseconds - ending["Length"] ) < 150:
          found=found+1
        else:
          break
      if found == len(ending):
        return True
      else:
        return False
    #-----------------------------------------------------------------------------------------------
     
    def setReceivePin( self, PinNo ):
        receivePin=PinNo
        
    #-----------------------------------------------------------------------------------------------
           
    def protocolName( self ):
      protNames=[]
      for prot in self.protocolInfo["Protocols"]: 
        protNames.append(prot["ProtocolName"])
      return protNames    

    #-----------------------------------------------------------------------------------------------

    def getReceivedRaw( self, secondsToRun):
          ReceivedRaw = [[], []]
          signal=self.LOW
          cumulative_time =0
          start_time=datetime.now()
          while cumulative_time < secondsToRun:
              beginning_time = datetime.now()
              while GPIO.input(self.receivePin) == signal:
                pass
              timeDelta= (datetime.now() - beginning_time).microseconds
              ReceivedRaw[0].append(timeDelta)
              ReceivedRaw[1].append(signal)
                
              cumulative_time = (datetime.now() - start_time).seconds
              signal=int(not(signal))
     
          GPIO.cleanup()
          return ReceivedRaw
   
    #-----------------------------------------------------------------------------------------------
   
    def SetupPin(self,mode="receive"):
      GPIO.setmode(GPIO.BCM)
      if (mode=="receive"):
        GPIO.setup(self.receivePin, GPIO.IN)
      elif (mode=="transmit"):
        GPIO.setup(self.transmitPin, GPIO.OUT)
        GPIO.output(self.transmitPin, GPIO.LOW)
      else:
        print ("Error: unknown mode - should be either 'receive' or 'transmit'")

    #-----------------------------------------------------------------------------------------------
    
    def CleanupPins(self):
      GPIO.cleanup()

    #-----------------------------------------------------------------------------------------------
            
    def ReceiveMessages( self ):         
          ValidNumber=False
          protocol = self.__GetLatch()
          if (protocol is not None):
            number=self.__ReadDigits(protocol)
            if len(number)==protocol["CodeBlock"]["Body"]["NumberOfBits"]:
              ValidNumber=True
            self.__DoEnding
            if ValidNumber:
              return number 
    #-----------------------------------------------------------------------------------------------
 
    def TransmitMessages(self, protocol, number):
          #Quck conversion from seconds to microseconds
          usleep = lambda x: time.sleep(x/1000000.0)
          
          reqProtocol=None
          timings=[]
          
          #Get the correct required protocol from the json
          for prot in self.protocolInfo["Protocols"]:
            if prot["ProtocolName"]==protocol:
              reqProtocol=prot["CodeBlock"]
              break
          
          if reqProtocol==None:
            print("Error: Requested protocol not found")
            exit(0)
          
          #Get everything ready before we fire off. Timing is of the essence
          Latch=reqProtocol["Latch"]
          Ending=reqProtocol["Ending"]  
          NoTimesToSend=reqProtocol["NumberOfBlocksToSend"]
          NoOfBits = reqProtocol["Body"]["NumberOfBits"]
          
          #Save the timings for 0 and 1 to a list since it is easier to access
          for digit in reqProtocol["Body"]["Digit"]:
              timings.append(digit["timings"])
          
          #Create mask to isolate the leftmost bit ie 1000000000000000 etc
          mask = mask = 1 << NoOfBits - 1      
          
          savedNumber=number
          
          
          #Code is sent a number of times in case it isn't picked up
          for i in range(1,NoTimesToSend):
             number= savedNumber
             
             #First send the latch
             for Block in Latch:
               GPIO.output(self.transmitPin, Block["Signal"])
               usleep(Block["Length"]-100)  
             
             #Get the code bit by bit
             for i in range(0,NoOfBits):
               bit = int(number & mask == mask)
             
               for Block in timings[bit]:
                 GPIO.output(self.transmitPin, Block["Signal"])
                 usleep(Block["Length"])
               #Rotate the number so we get the next bit
               number = number << 1
             
             #Do the end-up 
             for Block in Ending:
                GPIO.output(self.transmitPin, Block["Signal"])
                usleep(Block["Length"])  
#-----------------------------------------------------------------------------------------------
    

if __name__ == "__main__":
  # test receive
  secondsToRun=10
  lRFCtrl = RFCtrl()
  lRFCtrl.SetupPin()
  cumulative_time=0
  start_time=datetime.now()
  while cumulative_time < secondsToRun:
    number= lRFCtrl.ReceiveMessages()
    if number is not None and not(number==''):
      print (int(number,2))
    cumulative_time = (datetime.now() - start_time).seconds

  lRFCtrl.CleanupPins()

