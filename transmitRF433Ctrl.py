#!/usr/bin/env python
#-------------------------------------------------------------------------------
# Name:        Transmit RF 433 mhz signals
#              
# Purpose:     Class to Transmit RF 433 mhz signals using
#              different protocols for the Raspberry pi
#
# Author:      Dani Thomas
#
# Usage:       Call with the name of the switch operation you wish to fire
#              eg python transmitRF433Ctrl.py -switch LivingRoomLight -value on
# Requires:    
# Based on:    
#-------------------------------------------------------------------------------


import argparse
import os.path
import json
import sys

import RF433Ctrl

class transmitRF433:

  HOME_AUTOMATION_FILENAME=os.path.dirname(os.path.realpath(__file__)) + "/homeautomation.json"
  homeAutomationInfo=None
  
  def __init__( self ):
    self.__tryToLoadHomeAutomationFile()
    self.lRFCtrl = RF433Ctrl.RFCtrl()
    
  def __tryToLoadHomeAutomationFile(self):
    print (self.HOME_AUTOMATION_FILENAME)
    if os.path.exists( self.HOME_AUTOMATION_FILENAME ):
      try:
        with open( self.HOME_AUTOMATION_FILENAME ) as homeAutomationFile:
          self.homeAutomationInfo = json.load( homeAutomationFile )                             
      except Exception as e:
        print( "Unable to load home automation file. Exception was " + str( e ) )
    else:
      print("Unable to find path " + self.HOME_AUTOMATION_FILENAME)
      
  def transmitCode(self,switch,value):
    protocol,code = self.getSwitchCode(switch, value)
    print(protocol)
    self.lRFCtrl.SetupPin("transmit")
    print('Sending Code')
    self.lRFCtrl.TransmitMessages(protocol, code)
    self.lRFCtrl.CleanupPins()
    
  def getSwitchCode(self,reqSwitch,reqValue):
    for switch in self.homeAutomationInfo["switches"]:
      if switch["switch"] == reqSwitch:
        protocol=switch["protocol"]
        code=int(switch["value"][reqValue])
        break
    return (protocol,code)
  
if __name__ == "__main__":
  ap = argparse.ArgumentParser()
  ap.add_argument("-switch", action='store', help="the switch operation you want to fire")
  ap.add_argument("-value", action='store', help="the value whether on or off")
  args=ap.parse_args()
  
  homeAutomat= transmitRF433()
  
  if args.switch is None or args.value is None:
     print("You must use the command line parameters -switch LivingRoomLight -value on")
     exit(0)
 
  homeAutomat.transmitCode(args.switch,args.value)
  